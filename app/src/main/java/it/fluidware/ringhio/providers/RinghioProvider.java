package it.fluidware.ringhio.providers;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import it.fluidware.aahc.AAHC;
import it.fluidware.aahc.impl.JSONObjectResponse;
import it.fluidware.ringhio.R;

/**
 * Created by macno on 12/07/16.
 */
public class RinghioProvider {

    private static final String NOTIFY_PATH = "/api/v1/notify";


    private Context mContext;

    public RinghioProvider(Context context) {
        mContext = context;
    }


    public void sendStatus(String phoneNumber, String pin, JSONObject data) {


        final String url = mContext.getString(R.string.base_url) + NOTIFY_PATH;

        final JSONObject _data = new JSONObject();

        try {
            _data
                    .put("phone", phoneNumber)
                    .put("pin", pin)
                    .put("data", data);

        } catch (JSONException e) {
            Log.e("RINGHIO", "Failed to put data", e);
            return;
        }


        AAHC
                .use(mContext)
                .toPost(url, "application/json", _data.toString())
                .whenError(new AAHC.ErrorListener() {
                    @Override
                    public void onError(Exception e) {
                        Log.e("RINGHIO", "Wops " + e.getMessage(), e);
                    }
                })
                .into(new JSONObjectResponse() {
                    @Override
                    public void done(JSONObject obj) {
                        Log.d("RINGHIO", "Got: " + obj.toString());
                    }
                });
    }

}
