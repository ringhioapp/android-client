package it.fluidware.ringhio.managers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by macno on 13/07/16.
 */
public class PermissionManager {

    public static boolean isReadContactsGranted(Context context) {
        return isPermissionGranted(context,Manifest.permission.READ_CONTACTS);
    }

    public static boolean isReadPhoneStateGranted(Context context) {
        return isPermissionGranted(context,Manifest.permission.READ_PHONE_STATE);
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        int permissionCheck = ContextCompat.checkSelfPermission(context,
                permission);
        return (permissionCheck == PackageManager.PERMISSION_GRANTED);
    }
}
