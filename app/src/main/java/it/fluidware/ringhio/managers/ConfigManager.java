package it.fluidware.ringhio.managers;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Random;

/**
 * Created by macno on 12/07/16.
 */
public class ConfigManager {

    private static final String K_VALID_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private static final String PREF_NAME = "prefConfig";

    private static final String K_SECRET_PIN = "secretPIN";
    private static final String K_PHONE_NUMBER = "phoneNumber";


    private static ConfigManager mInstance;

    private SharedPreferences mPreferences;

    private ConfigManager(Context context) {
        mPreferences =
                context.getSharedPreferences(PREF_NAME,
                        context.MODE_PRIVATE);
    }

    public static synchronized ConfigManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ConfigManager(context);
        }
        return mInstance;
    }

    public String getSecretPin() {
        return mPreferences.getString(K_SECRET_PIN, null);
    }

    public void setSecretPin(String pin) {
        mPreferences
                .edit()
                .putString(K_SECRET_PIN, pin)
                .commit();
    }

    public String generateSecretPin() {
        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        for (int i = 0; i < 6; i++) {
            int p = r.nextInt(K_VALID_CHARS.length());
            sb.append(K_VALID_CHARS.charAt(p));
        }
        String pin = sb.toString();
        setSecretPin(pin);
        return pin;
    }

    public String getPhoneNumber() {
        return mPreferences.getString(K_PHONE_NUMBER, null);
    }

    public void setPhoneNumber(String number) {
        mPreferences
                .edit()
                .putString(K_PHONE_NUMBER, number)
                .commit();
    }


}
