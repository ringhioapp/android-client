package it.fluidware.ringhio.receivers;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import it.fluidware.ringhio.managers.ConfigManager;
import it.fluidware.ringhio.managers.PermissionManager;
import it.fluidware.ringhio.providers.RinghioProvider;

/**
 * Created by macno on 12/07/16.
 */
public class PhoneReceiver extends BroadcastReceiver {

    private RinghioProvider mProvider;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (mProvider == null) {
            mProvider = new RinghioProvider(context);
        }

        Log.d("RINGHIO", "onReceive");
        ConfigManager configManager = ConfigManager.getInstance(context);

        String currentPin = configManager.getSecretPin();
        if (currentPin == null) {
            return;
        }

        String phoneNumber = configManager.getPhoneNumber();


        String status = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        Log.d("RINGHIO", "state is " + status);
        JSONObject data = new JSONObject();

        try {
            data.put("status", status);
            if (status.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                Log.d("RINGHIO", "caller is " + number);
                data.put("caller", number);

                if(PermissionManager.isReadContactsGranted(context)) {
                    Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
                    Cursor cursor = context.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null,
                            null,
                            null);
                    if (cursor.moveToNext()) {
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                        if (name != null && !"".equals(name)) {
                            data.put("caller_name", name);
                        }
                    }
                    cursor.close();
                }

            }

        } catch (JSONException e) {
            Log.e("RINGHIO", "Failed to put caller", e);
        }


        mProvider.sendStatus(phoneNumber, currentPin, data);

    }


}
