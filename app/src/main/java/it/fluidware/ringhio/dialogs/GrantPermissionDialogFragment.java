package it.fluidware.ringhio.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import it.fluidware.ringhio.R;
import it.fluidware.ringhio.interfaces.PromptGrantPermissionInterface;

/**
 * Created by macno on 12/07/16.
 */
public class GrantPermissionDialogFragment extends DialogFragment {

    private PromptGrantPermissionInterface mCallback;

    private int mMessage = R.string.grant_phone_status_why;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(mMessage)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mCallback.onAccept();

                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        mCallback.onRefuse();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setMessage(int message) {
        mMessage = message;
    }

    @Override
    public void onAttach(Activity activity) {
        try {
            mCallback = (PromptGrantPermissionInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PromptGrantPermissionInterface");
        }
        super.onAttach(activity);
    }
}
