package it.fluidware.ringhio.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import it.fluidware.ringhio.R;
import it.fluidware.ringhio.dialogs.GrantPermissionDialogFragment;
import it.fluidware.ringhio.interfaces.PromptGrantPermissionInterface;
import it.fluidware.ringhio.managers.ConfigManager;
import it.fluidware.ringhio.managers.PermissionManager;

public class MainActivity extends AppCompatActivity implements PromptGrantPermissionInterface {

    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 2;

    private EditText mEditTextPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditTextPhone = (EditText)findViewById(R.id.et_phone_number);

        mEditTextPhone.setText(ConfigManager.getInstance(this).getPhoneNumber());

        setTextPin();

    }



    private void setTextPin() {

        String currentPin = ConfigManager.getInstance(this).getSecretPin();
        if(currentPin == null) {
            findViewById(R.id.ll_pin).setVisibility(View.INVISIBLE);
        } else {
            findViewById(R.id.ll_pin).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.tv_current_pin)).setText(currentPin);
        }
    }

    public void generateNewPin(View view) {

        if(!PermissionManager.isReadPhoneStateGranted(this)) {
            requestGrantPhone();
            return;
        }
        if(!PermissionManager.isReadContactsGranted(this)) {
            requestGrantPhone();
            return;
        }
        String phoneNumber = mEditTextPhone.getText().toString();
        if(phoneNumber.equals("")) {
            Toast.makeText(MainActivity.this, R.string.insert_phone_number, Toast.LENGTH_SHORT).show();
            return;
        }
        ConfigManager configManager = ConfigManager.getInstance(this);
        configManager.setPhoneNumber(phoneNumber);
        configManager.generateSecretPin();
        setTextPin();
    }



    public void requestGrantPhone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {

            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

            GrantPermissionDialogFragment dialogFragment = new GrantPermissionDialogFragment();
            dialogFragment.show(getFragmentManager(), "grantLocationFragment");

        } else {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);

        }
    }

    public void requestGrantContact() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS)) {

            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

            GrantPermissionDialogFragment dialogFragment = new GrantPermissionDialogFragment();
            dialogFragment.setMessage(R.string.grant_contact_why);
            dialogFragment.show(getFragmentManager(), "grantContactFragment");

        } else {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if(!PermissionManager.isReadContactsGranted(this)) {
                        requestGrantContact();
                    } else {
                        generateNewPin(null);
                    }

                }

            }
            break;

            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if(!PermissionManager.isReadPhoneStateGranted(this)) {
                        requestGrantPhone();
                    } else {
                        generateNewPin(null);
                    }

                }

            }
            break;

        }
    }


    @Override
    public void onAccept() {

        if(!PermissionManager.isReadPhoneStateGranted(this)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else if(!PermissionManager.isReadContactsGranted(this)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }

    }

    @Override
    public void onRefuse() {

    }
}
