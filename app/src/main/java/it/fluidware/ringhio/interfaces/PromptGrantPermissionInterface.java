package it.fluidware.ringhio.interfaces;

/**
 * Created by macno on 12/07/16.
 */
public interface PromptGrantPermissionInterface {

    void onAccept();
    void onRefuse();

}
